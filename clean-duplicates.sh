#!/bin/bash
shopt -s globstar

show_help() {
cat << EOF
  Usage: ${0##*/} [-h] [-c] "cache_file" [-d] "duplicates" [-s] "source_dir"
      -c cache_file      Cache-File [Default: .cleaner-cache]
      -d duplicates      Destination for Duplicates [Default: duplicates/]
      -s source_dir      Source Directory [Default "."]
      -e extensions      Comma separated list of extensions [Default "mp3, mp4"]
      -d                 Dry Run
      -h                 Show this context
EOF
}

write_cache() {
  if [ -f "${CACHE_FILE}" ]; then
    echo "$1" >> "${CACHE_FILE}"
  else
    echo "$1" > "${CACHE_FILE}"
  fi
}

## -- Default Environments Variables
SOURCE_DIRECTORY="."
DESTINATION="duplicates/"
CACHE_FILE=".cleaner-cache"
EXTENSIONS="mp3, mp4"

## -- Opting arguments
OPTIND=1; # Reset OPTIND, to clear getopts when used in a prior script
while getopts ":hs:o:c:dve:" opt; do
  case ${opt} in
    s)
       SOURCE_DIRECTORY="${OPTARG}";
       ;;
    o)
       DESTINATION="${OPTARG}";
       ;;
    c)
       CACHE_FILE="${OPTARG}";
       ;;
    v)
   	   DEBUG="true";
   	   ;;
    e)
       EXTENSIONS="${OPTARG}";
       ;;
    h)
       show_help
       exit 0
       ;;
    ?)
       echo "Invalid Option: -$OPTARG" 1>&2
       exit 1
       ;;
  esac
done
shift $((OPTIND -1))

## -- Check Destination/Source/Cache
! [[ -d "${DESTINATION}" ]] && mkdir $DESTINATION
if ! [ -d "${SOURCE_DIRECTORY}" ]; then
   echo "Directory not found ${SOURCE_DIRECTORY}" && exit 1;
fi
if [[ "$DRY_RUN" == "true" ]]; then
   [[ -f "${DESTINATION%/}dry-run.txt" ]] && rm -f "${DESTINATION%/}dry-run.txt"
   touch "${DESTINATION%/}dry-run.txt"
fi
[[ -f $CACHE_FILE ]] && rm -f ${CACHE_FILE}
touch $CACHE_FILE


if [[ $EXTENSIONS == *","* ]]; then
  IFS=',' read -ra EXT <<< "$EXTENSIONS"
  for extension in ${EXT[@]}; do
#     
    STATEMENT="$STATEMENT  -iname '*.$extension' -type f -print0"	
    echo "${EXT[-1]}"
	echo "${extension}"
    ! [[ $(echo "${EXT[-1]}" | sed 's/ //g') == *"${extension}"* ]] && STATEMENT="$STATEMENT -or"
  done
#
#
  echo "It's there!"
else
  STATEMENT="-iname '*.${EXTENSIONS}' -type f -print0"
fi

echo "$STATEMENT"



find $SOURCE_DIRECTORY $STATEMENT | while read -d '\0' file
do
  [[ -f "$file" ]] || continue
  read checksum _ < <(md5sum "$file")
  cache="$checksum;;$file"

  FILE_RESULT=$(grep "$checksum" "${CACHE_FILE}")
  if ! [[ -z "${FILE_RESULT}" ]]; then
     [[ "${DEBUG}" == "true" ]] && echo "Duplicate: $checksum"
     #IFS=';;' read -ra CACHE_OBJ <<< "$FILE_RESULT"


     #mv $file ${DESTINATION%/}/${file##*/}
     echo "delete $file"
     write_cache "$cache"
  else
    [[ "${DEBUG}" == "true" ]] && echo "Writing to cache: $file"
    write_cache "$cache"
  fi
done

## -- Overview
DUPLICATES=$(($(ls -ah ${DESTINATION} | wc -l) -2))
FILES=$(cat "${CACHE_FILE}" | wc -l || true)

echo -e "----------------------\nDeleted Files: $DUPLICATES\nScanned Files: $FILES\n----------------------"
rm -f "${CACHE_FILE}" # Remove Cache
